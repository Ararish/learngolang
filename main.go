package main

import (
	"fmt"
	"log"
	"net/http"
)

func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome to the HomePage!")
	fmt.Println("Endpoint Hit: homePage")
}

func testPage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "This is a test page for testing test")
}

func handleRequests() {
	http.HandleFunc("/", homePage)
	http.HandleFunc("/test", testPage)
	log.Fatal(http.ListenAndServe(":8888", nil))
}

func main() {
	handleRequests()
}
